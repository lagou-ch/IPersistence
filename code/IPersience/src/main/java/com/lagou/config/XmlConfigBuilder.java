package com.lagou.config;

import com.lagou.io.Resource;
import com.lagou.pojo.Configuration;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 *  创建xml解析对象
 *
 * @author Administrator
 */
@SuppressWarnings("all")
public class XmlConfigBuilder {

    private Configuration configuration;

    public XmlConfigBuilder() {
        this.configuration = new Configuration();
    }

    /**
     *  使用dom4j解析xml文件内容，封装为Configuration
     *
     * @param inputStream 配置文件输入流
     * @return Configuration
     */
    public Configuration parseConfig(InputStream inputStream) throws DocumentException, PropertyVetoException {
        // 数据源进行解析，设值
        Document document = new SAXReader().read(inputStream);
        // 获取根目录
        Element rootElement = document.getRootElement();
        List<Element> list = rootElement.selectNodes("//property");

        Properties props = new Properties();
        /**
         *  获取到数据源的xml配置信息
         */
        for (Element element : list) {
            String name = element.attributeValue("name");
            String value = element.attributeValue("value");
            props.setProperty(name,value);
        }

        ComboPooledDataSource comboPooledDataSource = new ComboPooledDataSource();
        comboPooledDataSource.setDriverClass(props.getProperty("driverClass"));
        comboPooledDataSource.setJdbcUrl(props.getProperty("jdbcUrl"));
        comboPooledDataSource.setUser(props.getProperty("username"));
        comboPooledDataSource.setPassword(props.getProperty("password"));

        configuration.setDataSource(comboPooledDataSource);

        // mapper.xml的解析
        List<Element> mapperElements = rootElement.selectNodes("//mapper");
        for (Element mapperElement : mapperElements) {
            String mapperPath = mapperElement.attributeValue("resource");
            InputStream resourceAsSteam = Resource.getResourceAsSteam(mapperPath);
            XmlMapperBuilder xmlMapperBuilder = new XmlMapperBuilder(configuration);
            xmlMapperBuilder.parse(resourceAsSteam);
        }

        return configuration;
    }
}
