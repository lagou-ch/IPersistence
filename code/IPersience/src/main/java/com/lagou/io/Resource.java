package com.lagou.io;


import java.io.InputStream;

/**
 * 资源类
 *
 * @author Administrator
 */
public class Resource {


    /**
     * 创建输入流，将xml文件进行流式输入，放置后续处理
     *
     * @param path 输入文件路径
     * @return InputStream  InputStream
     */
    public static InputStream getResourceAsSteam(String path){
        // getClassLoader().getResourceAsStream(...), 这里获取到的跟路径下的相对资源的路径，不用加在"/"
        InputStream resourceAsStream = Resource.class.getClassLoader().getResourceAsStream(path);
        return resourceAsStream;
    }
}
