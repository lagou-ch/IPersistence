package com.lagou.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 配置类的封装对象
 *
 * @author Administrator
 */
@SuppressWarnings("all")
public class Configuration {
    /**
     * 数据源对象
     */
    private DataSource dataSource;

    /**
     * 封装好的sqlMapperStatement对象
     */
    private Map<String, MappedStatement> sqlMappedStatementMap = new HashMap<>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getSqlMappedStatementMap() {
        return sqlMappedStatementMap;
    }

    public void setSqlMappedStatementMap(Map<String, MappedStatement> sqlMappedStatementMap) {
        this.sqlMappedStatementMap = sqlMappedStatementMap;
    }
}
