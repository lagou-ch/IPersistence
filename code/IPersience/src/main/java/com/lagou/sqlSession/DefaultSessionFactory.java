package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;


/**
 * 默认的工厂类实现
 *
 * @author Administrator
 */
public class DefaultSessionFactory implements SqlSessionFactory{

    private Configuration config;

    public DefaultSessionFactory(Configuration configuration) {
        this.config = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSession(config);
    }
}
