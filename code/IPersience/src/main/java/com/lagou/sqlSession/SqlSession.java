package com.lagou.sqlSession;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

/**
 * 具体的执行方法
 *
 * @author Administrator
 */
@SuppressWarnings("all")
public interface SqlSession{

    // 查询所有
    public <E>  List<E> selectList(String statementid, Object...params) throws ClassNotFoundException, SQLException, IllegalAccessException, NoSuchFieldException, InvocationTargetException, IntrospectionException, InstantiationException;

    // 查询单个
    public <T> T selectOne(String statementid, Object...params) throws ClassNotFoundException, SQLException, NoSuchFieldException, IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException;

    // 新增数据
    public void insert(String statementid, Object...params) throws ClassNotFoundException, SQLException, NoSuchFieldException, IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException;

    // 修改数据
    public void update(String statementid, Object...params) throws ClassNotFoundException, SQLException, NoSuchFieldException, IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException;

    // 删除方法
    public void delete(String statementid, Object...params) throws ClassNotFoundException, SQLException, NoSuchFieldException, IllegalAccessException, InstantiationException, IntrospectionException, InvocationTargetException;

    // 为dao层实现代理对象，进行方法的调用
    public <T> T getMapperClass(Class<?> mapperClass);

}
