package com.lagou.sqlSession;

/**
 * SqlSessionFactory工厂
 *
 * @author Administrator
 */
public interface SqlSessionFactory {

    /**
     * openSession获取sqlSession
     *
     * @return SqlSession
     */
    public SqlSession openSession();
}
