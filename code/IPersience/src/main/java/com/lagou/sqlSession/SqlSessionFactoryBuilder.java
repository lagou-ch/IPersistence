package com.lagou.sqlSession;

import com.lagou.config.XmlConfigBuilder;
import com.lagou.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * SqlSessionFactory构造器
 *
 * @author Administrator
 */
@SuppressWarnings("all")
public class SqlSessionFactoryBuilder {

    public SqlSessionFactory builder(InputStream  in) throws PropertyVetoException, DocumentException {
        // 使用dom4j进行解析流，将解析出来的内容封装到configuration中
        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfig(in);
        // 创建SqlSessionFactory对象
        DefaultSessionFactory defaultSessionFactory = new DefaultSessionFactory(configuration);
        return defaultSessionFactory;
    }

}
