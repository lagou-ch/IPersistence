package com.lagou.dao;

import com.lagou.pojo.User;

import java.util.List;

/**
 * 保证和xml文件的命名空间和方法名一致性，方便接口代理对象的invoke
 *
 */
public interface IUserDao {

    public List<User> selectAll() throws Exception;

    public User selectOne(User user) throws Exception;

    public void insert(User user) throws Exception;

    public void update(User user) throws Exception;

    public void delete(Integer id) throws Exception;

}
