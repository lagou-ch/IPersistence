package com.lagou.test;

import com.lagou.dao.IUserDao;
import com.lagou.io.Resource;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * 测试类
 *
 * @author Administrator
 */
@SuppressWarnings("all")
public class IPersienceTest {

    @Test
    public void test() throws Exception {
        InputStream resourceAsSteam = Resource.getResourceAsSteam("sqlMapConfig.xml");
        SqlSession sqlSession = new SqlSessionFactoryBuilder().builder(resourceAsSteam).openSession();

       /* User user = new User();
        user.setName("zs");
        user.setId(1);
        User user2 = (User)sqlSession.selectOne("user.selectOne", user);
        System.out.println(user2);*/

       /* List<User> users = sqlSession.selectList("user.selectAll");
        for (User user : users) {
            System.out.println(user);
        }*/

        IUserDao mapperClass = sqlSession.getMapperClass(IUserDao.class);
        // mapperClass代理对象是对接口的代理，执行的不管什么方法，都是invoke
//        User user = new User();
//        user.setName("zs5--7--8");
//        user.setId(4);
//        mapperClass.insert(user);
//        mapperClass.update(user);
        mapperClass.delete(3);
       /* for (User user : users1) {
            System.out.println(user);
        }*/
    }

}
